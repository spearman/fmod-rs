use fmod::*;

#[allow(unused_macros)]
macro_rules! show {
  ($e:expr) => { println!("{}: {:?}", stringify!($e), $e); }
}

#[allow(unused_macros)]
macro_rules! hex {
  ($e:expr) => { println!("{}: {:x}", stringify!($e), $e); }
}


fn main() {
  println!("fmod example main...");

  let mut system     = System::default().unwrap();
  hex!(fmod::FMOD_VERSION);
  hex!(system.get_version().unwrap());

  let sound_filename = "bite.wav";
  println!("  opening {:?}", sound_filename);
  let mut sound      = system.create_sound_from_file_default (sound_filename)
    .unwrap();
  println!("  playing {:?}", sound_filename);
  let channel        = sound.play (None, false).unwrap();
  while channel.is_playing().unwrap() {
    std::thread::sleep (std::time::Duration::from_millis (100));
  }
  println!("...fmod example main");
}
