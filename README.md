# `fmod`

> Incomplete Rust interface for the FMOD 1.10.18 (FMOD 5) Low Level API

*NOTE*: running the example on NixOS with pulseaudio seems to have problems;
after running and exiting the program, attempting to run again will fail to
create the FMOD system with `FileNotfound` error; run `$ pulseaudio -k` to kill
pulseaudio and then run the program again should automatically restart
pulseaudio and then the example should work
