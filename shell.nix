with import <nixpkgs> {};
mkShell {
  buildInputs = [
    gdb
    graphviz
    llvm
    rustup
    rust-analyzer
  ];
  # needed for fmod-sys crate so bindgen can find libclang.so
  LIBCLANG_PATH="${llvmPackages.libclang.lib}/lib";
  # needed for libfmod.so to dynamically link libstdc++.so.6 and libalsa.so
  LD_LIBRARY_PATH="${stdenv.cc.cc.lib}/lib:${alsaLib}/lib";
}
