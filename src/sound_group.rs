use std::{self, rc::Rc};
use log;
use crate::{ll, fmod_result, System};

#[derive(Clone, Debug, PartialEq)]
pub struct SoundGroup {
  inner  : Rc <Inner>,
  system : System
}

#[derive(PartialEq)]
struct Inner (*mut ll::FMOD_SOUNDGROUP);

impl SoundGroup {
  #[inline]
  pub fn from_raw_parts (raw : *mut ll::FMOD_SOUNDGROUP, system : System)
    -> Self
  {
    let inner = Rc::new (Inner (raw));
    SoundGroup { inner, system }
  }

  #[inline]
  #[allow(dead_code)]
  fn raw (&self) -> *mut ll::FMOD_SOUNDGROUP {
    (*self.inner).0
  }

  #[inline]
  pub (crate) fn raw_mut (&mut self) -> *mut ll::FMOD_SOUNDGROUP {
    (*self.inner).0
  }
}

impl std::fmt::Debug for Inner {
  fn fmt (&self, f : &mut std::fmt::Formatter) -> std::fmt::Result {
    write!(f, "{:p}", self.0)
  }
}

impl Drop for Inner {
  fn drop (&mut self) {
    unsafe {
      let _ = fmod_result!(ll::FMOD_SoundGroup_Release (self.0)).map_err (
        |err| log::error!("error releasing FMOD SoundGroup@{:p}: {:?}", self.0, err)
      );
    }
  }
}
