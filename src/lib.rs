#![feature(decl_macro)]

#[macro_use] extern crate bitflags;
#[macro_use] extern crate lazy_static;

extern crate num_traits;

pub use num_traits::FromPrimitive;

pub use fmod_sys as ll;

#[cfg(target_os = "linux")]
#[link(name = "fmod")] extern {}

#[cfg(target_os = "windows")]
#[link(name = "fmod64_vc")] extern {}

use lazy_static::lazy_static;

////////////////////////////////////////////////////////////////////////////////
//  modules                                                                   //
////////////////////////////////////////////////////////////////////////////////

pub mod channel;
pub mod channel_control;
pub mod channel_group;
pub mod dsp;
pub mod reverb3d;
pub mod sound;
pub mod sound_group;
pub mod system;

pub use self::channel::Channel;
pub use self::channel_control::ChannelControl;
pub use self::channel_group::{ChannelGroup, ChannelGroupRef};
pub use self::dsp::{Dsp, DspRef};
pub use self::reverb3d::Reverb3d;
pub use self::sound::{Sound, SoundRef};
pub use self::sound_group::SoundGroup;
pub use self::system::System;
// private modules
mod types;
pub use self::types::{Channelmask, CpuUsage, Delay, DebugFlags, DriverState,
  Error, Guid, ListenerAttributes, Mode, PluginHandle, Plugintype, SoundRam,
  Speaker, Speakermode, Timeunit};

/// Version in FMOD headers used to generate bindings.
///
/// The version is a 32 bit hexadecimal value formated as 16:8:8, with the upper
/// 16 bits being the product version, the middle 8 bits being the major version
/// and the bottom 8 bits being the minor version.
pub const FMOD_VERSION             : u32 = ll::FMOD_VERSION;
pub const CODEC_WAVEFORMAT_VERSION : u32 = ll::FMOD_CODEC_WAVEFORMAT_VERSION;
pub const MAX_CHANNEL_WIDTH        : u32 = ll::FMOD_MAX_CHANNEL_WIDTH;
pub const MAX_SYSTEMS              : u32 = ll::FMOD_MAX_SYSTEMS;
pub const MAX_LISTENERS            : u32 = ll::FMOD_MAX_LISTENERS;
pub const OUTPUT_PLUGIN_VERSION    : u32 = ll::FMOD_OUTPUT_PLUGIN_VERSION;
pub const PLUGIN_SDK_VERSION       : u32 = ll::FMOD_PLUGIN_SDK_VERSION;
lazy_static!{
  /// Version in FMOD headers used to generate bindings.
  pub static ref FMOD_VERSION_STRING : String = version_string (FMOD_VERSION);
}

pub (crate) macro fmod_result {
  ($e:expr) => {{
    match $e {
      $crate::ll::FMOD_RESULT_FMOD_OK => Ok (()),
      err => Err ($crate::Error::from_ll (err))
    }
  }}
}

pub mod vector {
  use crate::ll;
  pub const fn from_ll (vector : ll::FMOD_VECTOR) -> [f32; 3] {
    [vector.x, vector.y, vector.z]
  }
  pub const fn to_ll (vector : [f32; 3]) -> ll::FMOD_VECTOR {
    ll::FMOD_VECTOR { x: vector[0], y: vector[1], z: vector[2] }
  }
}

pub (crate) fn version_string (fmod_version : u32) -> String {
  format!("{:x}.{:x}.{:x}",
    (fmod_version & 0xffff0000) >> 16,
    (fmod_version & 0x0000ff00) >> 8,
    (fmod_version & 0x000000ff))
}
