use num_derive::FromPrimitive;
use crate::ll;

#[derive(Debug, Default, PartialEq)]
pub struct CpuUsage {
  pub dsp      : f32,
  pub stream   : f32,
  pub geometry : f32,
  pub update   : f32,
  pub total    : f32
}

#[derive(Clone, Debug, Default, Eq, PartialEq)]
pub struct Delay {
  pub dspclock_start : u64,
  pub dspclock_end   : u64,
  pub stopchannels   : bool
}

#[repr(C)]
#[derive(Debug, Eq, PartialEq)]
pub struct Guid {
  pub data1 : u32,
  pub data2 : u16,
  pub data3 : u16,
  pub data4 : [u8; 8]
}

#[derive(Debug, PartialEq)]
pub struct ListenerAttributes {
  pub pos     : [f32; 3],
  pub vel     : [f32; 3],
  pub forward : [f32; 3],
  pub up      : [f32; 3]
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct PluginHandle (pub u32);

#[derive(Debug, Default, Eq, PartialEq)]
pub struct SoundRam {
  pub currentalloced : i32,
  pub maxalloced     : i32,
  pub total          : i32
}

bitflags!{
  #[derive(Clone, Copy, Debug, Default, Eq, Ord, PartialEq, PartialOrd)]
  pub struct Channelmask : u32 {
    const FRONT_LEFT     = ll::FMOD_CHANNELMASK_FRONT_LEFT;
    const FRONT_RIGHT    = ll::FMOD_CHANNELMASK_FRONT_RIGHT;
    const FRONT_CENTER   = ll::FMOD_CHANNELMASK_FRONT_CENTER;
    const LOW_FREQUENCY  = ll::FMOD_CHANNELMASK_LOW_FREQUENCY;
    const SURROUND_LEFT  = ll::FMOD_CHANNELMASK_SURROUND_LEFT;
    const SURROUND_RIGHT = ll::FMOD_CHANNELMASK_SURROUND_RIGHT;
    const BACK_LEFT      = ll::FMOD_CHANNELMASK_BACK_LEFT;
    const BACK_RIGHT     = ll::FMOD_CHANNELMASK_BACK_RIGHT;
    const BACK_CENTER    = ll::FMOD_CHANNELMASK_BACK_CENTER;
    const MONO           = ll::FMOD_CHANNELMASK_MONO;
    const STEREO         = ll::FMOD_CHANNELMASK_STEREO;
    const LRC            = ll::FMOD_CHANNELMASK_LRC;
    const QUAD           = ll::FMOD_CHANNELMASK_QUAD;
    const SURROUND       = ll::FMOD_CHANNELMASK_SURROUND;
    const _5POINT1       = ll::FMOD_CHANNELMASK_5POINT1;
    const _5POINT1_REARS = ll::FMOD_CHANNELMASK_5POINT1_REARS;
    const _7POINT0       = ll::FMOD_CHANNELMASK_7POINT0;
    const _7POINT1       = ll::FMOD_CHANNELMASK_7POINT1;
  }
}

bitflags!{
  #[derive(Clone, Copy, Debug, Default, Eq, Ord, PartialEq, PartialOrd)]
  pub struct DebugFlags : u32 {
    const LEVEL_NONE          = ll::FMOD_DEBUG_LEVEL_NONE;
    const LEVEL_ERROR         = ll::FMOD_DEBUG_LEVEL_ERROR;
    const LEVEL_WARNING       = ll::FMOD_DEBUG_LEVEL_WARNING;
    const LEVEL_LOG           = ll::FMOD_DEBUG_LEVEL_LOG;
    const TYPE_MEMORY         = ll::FMOD_DEBUG_TYPE_MEMORY;
    const TYPE_FILE           = ll::FMOD_DEBUG_TYPE_FILE;
    const TYPE_CODEC          = ll::FMOD_DEBUG_TYPE_CODEC;
    const TYPE_TRACE          = ll::FMOD_DEBUG_TYPE_TRACE;
    const DISPLAY_TIMESTAMPS  = ll::FMOD_DEBUG_DISPLAY_TIMESTAMPS;
    const DISPLAY_LINENUMBERS = ll::FMOD_DEBUG_DISPLAY_LINENUMBERS;
    const DISPLAY_THREAD      = ll::FMOD_DEBUG_DISPLAY_THREAD;
  }
}

bitflags!{
  #[derive(Clone, Copy, Debug, Default, Eq, Ord, PartialEq, PartialOrd)]
  pub struct DriverState : u32 {
    const DRIVER_STATE_CONNECTED = ll::FMOD_DRIVER_STATE_CONNECTED;
    const DRIVER_STATE_DEFAULT   = ll::FMOD_DRIVER_STATE_DEFAULT;
  }
}

bitflags!{
  #[derive(Clone, Copy, Debug, Default, Eq, Ord, PartialEq, PartialOrd)]
  pub struct MemoryType : u32 {
    const NORMAL           = ll::FMOD_MEMORY_NORMAL;
    const STREAM_FILE      = ll::FMOD_MEMORY_STREAM_FILE;
    const STREAM_DECODE    = ll::FMOD_MEMORY_STREAM_DECODE;
    const SAMPLEDATA       = ll::FMOD_MEMORY_SAMPLEDATA;
    const DSP_BUFFER       = ll::FMOD_MEMORY_DSP_BUFFER;
    const PLUGIN           = ll::FMOD_MEMORY_PLUGIN;
    const XBOX360_PHYSICAL = ll::FMOD_MEMORY_XBOX360_PHYSICAL;
    const PERSISTENT       = ll::FMOD_MEMORY_PERSISTENT;
    const SECONDARY        = ll::FMOD_MEMORY_SECONDARY;
    const ALL              = ll::FMOD_MEMORY_ALL;
  }
}

bitflags!{
  #[derive(Clone, Copy, Debug, Default, Eq, Ord, PartialEq, PartialOrd)]
  pub struct Mode : u32 {
    const DEFAULT                   = ll::FMOD_DEFAULT;
    const LOOP_OFF                  = ll::FMOD_LOOP_OFF;
    const LOOP_NORMAL               = ll::FMOD_LOOP_NORMAL;
    const LOOP_BIDI                 = ll::FMOD_LOOP_BIDI;
    const _2D                       = ll::FMOD_2D;
    const _3D                       = ll::FMOD_3D;
    const CREATESTREAM              = ll::FMOD_CREATESTREAM;
    const CREATESAMPLE              = ll::FMOD_CREATESAMPLE;
    const CREATECOMPRESSEDSAMPLE    = ll::FMOD_CREATECOMPRESSEDSAMPLE;
    const OPENUSER                  = ll::FMOD_OPENUSER;
    const OPENMEMORY                = ll::FMOD_OPENMEMORY;
    const OPENMEMORY_POINT          = ll::FMOD_OPENMEMORY_POINT;
    const OPENRAW                   = ll::FMOD_OPENRAW;
    const OPENONLY                  = ll::FMOD_OPENONLY;
    const ACCURATETIME              = ll::FMOD_ACCURATETIME;
    const MPEGSEARCH                = ll::FMOD_MPEGSEARCH;
    const NONBLOCKING               = ll::FMOD_NONBLOCKING;
    const UNIQUE                    = ll::FMOD_UNIQUE;
    const _3D_HEADRELATIVE          = ll::FMOD_3D_HEADRELATIVE;
    const _3D_WORLDRELATIVE         = ll::FMOD_3D_WORLDRELATIVE;
    const _3D_INVERSEROLLOFF        = ll::FMOD_3D_INVERSEROLLOFF;
    const _3D_LINEARROLLOFF         = ll::FMOD_3D_LINEARROLLOFF;
    const _3D_LINEARSQUAREROLLOFF   = ll::FMOD_3D_LINEARSQUAREROLLOFF;
    const _3D_INVERSETAPEREDROLLOFF = ll::FMOD_3D_INVERSETAPEREDROLLOFF;
    const _3D_CUSTOMROLLOFF         = ll::FMOD_3D_CUSTOMROLLOFF;
    const _3D_IGNOREGEOMETRY        = ll::FMOD_3D_IGNOREGEOMETRY;
    const IGNORETAGS                = ll::FMOD_IGNORETAGS;
    const LOWMEM                    = ll::FMOD_LOWMEM;
    const LOADSECONDARYRAM          = ll::FMOD_LOADSECONDARYRAM;
    const VIRTUAL_PLAYFROMSTART     = ll::FMOD_VIRTUAL_PLAYFROMSTART;
  }
}

bitflags!{
  #[derive(Clone, Copy, Debug, Default, Eq, Ord, PartialEq, PartialOrd)]
  pub struct PortIndex : u64 {
    const NONE = std::u64::MAX;
  }
}

bitflags!{
  #[derive(Clone, Copy, Debug, Eq, Ord, PartialEq, PartialOrd)]
  pub struct Timeunit : u32 {
    const MS          = ll::FMOD_TIMEUNIT_MS;
    const PCM         = ll::FMOD_TIMEUNIT_PCM;
    const PCMBYTES    = ll::FMOD_TIMEUNIT_PCMBYTES;
    const RAWBYTES    = ll::FMOD_TIMEUNIT_RAWBYTES;
    const PCMFRACTION = ll::FMOD_TIMEUNIT_PCMFRACTION;
    const MODORDER    = ll::FMOD_TIMEUNIT_MODORDER;
    const MODROW      = ll::FMOD_TIMEUNIT_MODROW;
    const MODPATTERN  = ll::FMOD_TIMEUNIT_MODPATTERN;
  }
}

#[derive(Copy, Clone, Debug, Eq, PartialEq, FromPrimitive)]
pub enum Error {
  Badcommand              = ll::FMOD_RESULT_FMOD_ERR_BADCOMMAND                as isize,
  ChannelAlloc            = ll::FMOD_RESULT_FMOD_ERR_CHANNEL_ALLOC             as isize,
  ChannelStolen           = ll::FMOD_RESULT_FMOD_ERR_CHANNEL_STOLEN            as isize,
  Dma                     = ll::FMOD_RESULT_FMOD_ERR_DMA                       as isize,
  DspConnection           = ll::FMOD_RESULT_FMOD_ERR_DSP_CONNECTION            as isize,
  DspDontprocess          = ll::FMOD_RESULT_FMOD_ERR_DSP_DONTPROCESS           as isize,
  DspFormat               = ll::FMOD_RESULT_FMOD_ERR_DSP_FORMAT                as isize,
  DspInuse                = ll::FMOD_RESULT_FMOD_ERR_DSP_INUSE                 as isize,
  DspNotfound             = ll::FMOD_RESULT_FMOD_ERR_DSP_NOTFOUND              as isize,
  DspReserved             = ll::FMOD_RESULT_FMOD_ERR_DSP_RESERVED              as isize,
  DspSilence              = ll::FMOD_RESULT_FMOD_ERR_DSP_SILENCE               as isize,
  DspType                 = ll::FMOD_RESULT_FMOD_ERR_DSP_TYPE                  as isize,
  FileBad                 = ll::FMOD_RESULT_FMOD_ERR_FILE_BAD                  as isize,
  FileCouldnotseek        = ll::FMOD_RESULT_FMOD_ERR_FILE_COULDNOTSEEK         as isize,
  FileDiskejected         = ll::FMOD_RESULT_FMOD_ERR_FILE_DISKEJECTED          as isize,
  FileEof                 = ll::FMOD_RESULT_FMOD_ERR_FILE_EOF                  as isize,
  FileEndofdata           = ll::FMOD_RESULT_FMOD_ERR_FILE_ENDOFDATA            as isize,
  FileNotfound            = ll::FMOD_RESULT_FMOD_ERR_FILE_NOTFOUND             as isize,
  Format                  = ll::FMOD_RESULT_FMOD_ERR_FORMAT                    as isize,
  HeaderMismatch          = ll::FMOD_RESULT_FMOD_ERR_HEADER_MISMATCH           as isize,
  Http                    = ll::FMOD_RESULT_FMOD_ERR_HTTP                      as isize,
  HttpAccess              = ll::FMOD_RESULT_FMOD_ERR_HTTP_ACCESS               as isize,
  HttpProxyAuth           = ll::FMOD_RESULT_FMOD_ERR_HTTP_PROXY_AUTH           as isize,
  HttpServerError         = ll::FMOD_RESULT_FMOD_ERR_HTTP_SERVER_ERROR         as isize,
  HttpTimeout             = ll::FMOD_RESULT_FMOD_ERR_HTTP_TIMEOUT              as isize,
  Initialization          = ll::FMOD_RESULT_FMOD_ERR_INITIALIZATION            as isize,
  Initialized             = ll::FMOD_RESULT_FMOD_ERR_INITIALIZED               as isize,
  Internal                = ll::FMOD_RESULT_FMOD_ERR_INTERNAL                  as isize,
  InvalidFloat            = ll::FMOD_RESULT_FMOD_ERR_INVALID_FLOAT             as isize,
  InvalidHandle           = ll::FMOD_RESULT_FMOD_ERR_INVALID_HANDLE            as isize,
  InvalidParam            = ll::FMOD_RESULT_FMOD_ERR_INVALID_PARAM             as isize,
  InvalidPosition         = ll::FMOD_RESULT_FMOD_ERR_INVALID_POSITION          as isize,
  InvalidSpeaker          = ll::FMOD_RESULT_FMOD_ERR_INVALID_SPEAKER           as isize,
  InvalidSyncpoint        = ll::FMOD_RESULT_FMOD_ERR_INVALID_SYNCPOINT         as isize,
  InvalidThread           = ll::FMOD_RESULT_FMOD_ERR_INVALID_THREAD            as isize,
  InvalidVector           = ll::FMOD_RESULT_FMOD_ERR_INVALID_VECTOR            as isize,
  Maxaudible              = ll::FMOD_RESULT_FMOD_ERR_MAXAUDIBLE                as isize,
  Memory                  = ll::FMOD_RESULT_FMOD_ERR_MEMORY                    as isize,
  MemoryCantpoint         = ll::FMOD_RESULT_FMOD_ERR_MEMORY_CANTPOINT          as isize,
  Needs3d                 = ll::FMOD_RESULT_FMOD_ERR_NEEDS3D                   as isize,
  Needshardware           = ll::FMOD_RESULT_FMOD_ERR_NEEDSHARDWARE             as isize,
  NetConnect              = ll::FMOD_RESULT_FMOD_ERR_NET_CONNECT               as isize,
  NetSocketError          = ll::FMOD_RESULT_FMOD_ERR_NET_SOCKET_ERROR          as isize,
  NetUrl                  = ll::FMOD_RESULT_FMOD_ERR_NET_URL                   as isize,
  NetWouldBlock           = ll::FMOD_RESULT_FMOD_ERR_NET_WOULD_BLOCK           as isize,
  Notready                = ll::FMOD_RESULT_FMOD_ERR_NOTREADY                  as isize,
  OutputAllocated         = ll::FMOD_RESULT_FMOD_ERR_OUTPUT_ALLOCATED          as isize,
  OutputCreatebuffer      = ll::FMOD_RESULT_FMOD_ERR_OUTPUT_CREATEBUFFER       as isize,
  OutputDrivercall        = ll::FMOD_RESULT_FMOD_ERR_OUTPUT_DRIVERCALL         as isize,
  OutputFormat            = ll::FMOD_RESULT_FMOD_ERR_OUTPUT_FORMAT             as isize,
  OutputInit              = ll::FMOD_RESULT_FMOD_ERR_OUTPUT_INIT               as isize,
  OutputNodrivers         = ll::FMOD_RESULT_FMOD_ERR_OUTPUT_NODRIVERS          as isize,
  Plugin                  = ll::FMOD_RESULT_FMOD_ERR_PLUGIN                    as isize,
  PluginMissing           = ll::FMOD_RESULT_FMOD_ERR_PLUGIN_MISSING            as isize,
  PluginResource          = ll::FMOD_RESULT_FMOD_ERR_PLUGIN_RESOURCE           as isize,
  PluginVersion           = ll::FMOD_RESULT_FMOD_ERR_PLUGIN_VERSION            as isize,
  Record                  = ll::FMOD_RESULT_FMOD_ERR_RECORD                    as isize,
  ReverbChannelgroup      = ll::FMOD_RESULT_FMOD_ERR_REVERB_CHANNELGROUP       as isize,
  ReverbInstance          = ll::FMOD_RESULT_FMOD_ERR_REVERB_INSTANCE           as isize,
  Subsounds               = ll::FMOD_RESULT_FMOD_ERR_SUBSOUNDS                 as isize,
  SubsoundAllocated       = ll::FMOD_RESULT_FMOD_ERR_SUBSOUND_ALLOCATED        as isize,
  SubsoundCantmove        = ll::FMOD_RESULT_FMOD_ERR_SUBSOUND_CANTMOVE         as isize,
  Tagnotfound             = ll::FMOD_RESULT_FMOD_ERR_TAGNOTFOUND               as isize,
  Toomanychannels         = ll::FMOD_RESULT_FMOD_ERR_TOOMANYCHANNELS           as isize,
  Truncated               = ll::FMOD_RESULT_FMOD_ERR_TRUNCATED                 as isize,
  Unimplemented           = ll::FMOD_RESULT_FMOD_ERR_UNIMPLEMENTED             as isize,
  Uninitialized           = ll::FMOD_RESULT_FMOD_ERR_UNINITIALIZED             as isize,
  Unsupported             = ll::FMOD_RESULT_FMOD_ERR_UNSUPPORTED               as isize,
  Version                 = ll::FMOD_RESULT_FMOD_ERR_VERSION                   as isize,
  EventAlreadyLoaded      = ll::FMOD_RESULT_FMOD_ERR_EVENT_ALREADY_LOADED      as isize,
  EventLiveupdateBusy     = ll::FMOD_RESULT_FMOD_ERR_EVENT_LIVEUPDATE_BUSY     as isize,
  EventLiveupdateMismatch = ll::FMOD_RESULT_FMOD_ERR_EVENT_LIVEUPDATE_MISMATCH as isize,
  EventLiveupdateTimeout  = ll::FMOD_RESULT_FMOD_ERR_EVENT_LIVEUPDATE_TIMEOUT  as isize,
  EventNotfound           = ll::FMOD_RESULT_FMOD_ERR_EVENT_NOTFOUND            as isize,
  StudioUninitialized     = ll::FMOD_RESULT_FMOD_ERR_STUDIO_UNINITIALIZED      as isize,
  StudioNotLoaded         = ll::FMOD_RESULT_FMOD_ERR_STUDIO_NOT_LOADED         as isize,
  InvalidString           = ll::FMOD_RESULT_FMOD_ERR_INVALID_STRING            as isize,
  AlreadyLocked           = ll::FMOD_RESULT_FMOD_ERR_ALREADY_LOCKED            as isize,
  NotLocked               = ll::FMOD_RESULT_FMOD_ERR_NOT_LOCKED                as isize,
  RecordDisconnected      = ll::FMOD_RESULT_FMOD_ERR_RECORD_DISCONNECTED       as isize,
  Toomanysamples          = ll::FMOD_RESULT_FMOD_ERR_TOOMANYSAMPLES            as isize
}

#[derive(Copy, Clone, Debug, Eq, PartialEq, FromPrimitive)]
pub enum Plugintype {
  Output = ll::FMOD_PLUGINTYPE_FMOD_PLUGINTYPE_OUTPUT as isize,
  Codec  = ll::FMOD_PLUGINTYPE_FMOD_PLUGINTYPE_CODEC as isize,
  Dsp    = ll::FMOD_PLUGINTYPE_FMOD_PLUGINTYPE_DSP as isize,
  MAX    = ll::FMOD_PLUGINTYPE_FMOD_PLUGINTYPE_MAX as isize
}

#[derive(Copy, Clone, Debug, Eq, PartialEq, FromPrimitive)]
pub enum Speaker {
  FrontLeft     = ll::FMOD_SPEAKER_FMOD_SPEAKER_FRONT_LEFT      as isize,
  FrontRight    = ll::FMOD_SPEAKER_FMOD_SPEAKER_FRONT_RIGHT     as isize,
  FrontCenter   = ll::FMOD_SPEAKER_FMOD_SPEAKER_FRONT_CENTER    as isize,
  LowFrequency  = ll::FMOD_SPEAKER_FMOD_SPEAKER_LOW_FREQUENCY   as isize,
  SurroundLeft  = ll::FMOD_SPEAKER_FMOD_SPEAKER_SURROUND_LEFT   as isize,
  SurroundRight = ll::FMOD_SPEAKER_FMOD_SPEAKER_SURROUND_RIGHT  as isize,
  BackLeft      = ll::FMOD_SPEAKER_FMOD_SPEAKER_BACK_LEFT       as isize,
  BackRight     = ll::FMOD_SPEAKER_FMOD_SPEAKER_BACK_RIGHT      as isize,
  TopFrontLeft  = ll::FMOD_SPEAKER_FMOD_SPEAKER_TOP_FRONT_LEFT  as isize,
  TopFrontRight = ll::FMOD_SPEAKER_FMOD_SPEAKER_TOP_FRONT_RIGHT as isize,
  TopBackLeft   = ll::FMOD_SPEAKER_FMOD_SPEAKER_TOP_BACK_LEFT   as isize,
  TopBackRight  = ll::FMOD_SPEAKER_FMOD_SPEAKER_TOP_BACK_RIGHT  as isize,
  MAX           = ll::FMOD_SPEAKER_FMOD_SPEAKER_MAX             as isize
}

#[derive(Copy, Clone, Debug, Eq, PartialEq, FromPrimitive)]
pub enum Speakermode {
  Default        = ll::FMOD_SPEAKERMODE_FMOD_SPEAKERMODE_DEFAULT       as isize,
  Raw            = ll::FMOD_SPEAKERMODE_FMOD_SPEAKERMODE_RAW           as isize,
  Mono           = ll::FMOD_SPEAKERMODE_FMOD_SPEAKERMODE_MONO          as isize,
  Stereo         = ll::FMOD_SPEAKERMODE_FMOD_SPEAKERMODE_STEREO        as isize,
  Quad           = ll::FMOD_SPEAKERMODE_FMOD_SPEAKERMODE_QUAD          as isize,
  Surround       = ll::FMOD_SPEAKERMODE_FMOD_SPEAKERMODE_SURROUND      as isize,
  _5Point1       = ll::FMOD_SPEAKERMODE_FMOD_SPEAKERMODE_5POINT1       as isize,
  _7Point1       = ll::FMOD_SPEAKERMODE_FMOD_SPEAKERMODE_7POINT1       as isize,
  _7Point1Point4 = ll::FMOD_SPEAKERMODE_FMOD_SPEAKERMODE_7POINT1POINT4 as isize,
  MAX            = ll::FMOD_SPEAKERMODE_FMOD_SPEAKERMODE_MAX           as isize
}

impl Error {
  pub fn from_ll (ll : ll::FMOD_RESULT) -> Self {
    use num_traits::FromPrimitive;
    Self::from_u32 (ll as u32).unwrap()
  }
}

impl Guid {
  #[inline]
  pub fn as_mut_ll (&mut self) -> &mut ll::FMOD_GUID {
    debug_assert_eq!(
      std::mem::size_of::<Self>(),
      std::mem::size_of::<ll::FMOD_GUID>());
    unsafe { std::mem::transmute (self) }
  }
}

impl ListenerAttributes {
  #[inline]
  pub fn from_ll (
    pos     : ll::FMOD_VECTOR,
    vel     : ll::FMOD_VECTOR,
    forward : ll::FMOD_VECTOR,
    up      : ll::FMOD_VECTOR
  ) -> Self {
    let convert = |vec : ll::FMOD_VECTOR| [vec.x, vec.y, vec.z];
    let pos     = convert (pos);
    let vel     = convert (vel);
    let forward = convert (forward);
    let up      = convert (up);
    ListenerAttributes { pos, vel, forward, up }
  }
}

impl Speakermode {
  pub fn from_ll (ll : ll::FMOD_SPEAKERMODE) -> Self {
    use num_traits::FromPrimitive;
    Self::from_u32 (ll as u32).unwrap()
  }
}

impl Default for Timeunit {
  fn default() -> Self {
    Timeunit::empty()
  }
}
