use crate::{ll, Delay, Dsp, DspRef, Error, Mode};

pub enum Type {
  Channel      = ll::FMOD_CHANNELCONTROL_TYPE_FMOD_CHANNELCONTROL_CHANNEL
    as isize,
  Channelgroup = ll::FMOD_CHANNELCONTROL_TYPE_FMOD_CHANNELCONTROL_CHANNELGROUP
    as isize
}

pub enum CallbackType {
  End =
    ll::FMOD_CHANNELCONTROL_CALLBACK_TYPE_FMOD_CHANNELCONTROL_CALLBACK_END
      as isize,
  Virtualvoice =
    ll::FMOD_CHANNELCONTROL_CALLBACK_TYPE_FMOD_CHANNELCONTROL_CALLBACK_VIRTUALVOICE
      as isize,
  Syncpoint =
    ll::FMOD_CHANNELCONTROL_CALLBACK_TYPE_FMOD_CHANNELCONTROL_CALLBACK_SYNCPOINT
      as isize,
  Occlusion =
    ll::FMOD_CHANNELCONTROL_CALLBACK_TYPE_FMOD_CHANNELCONTROL_CALLBACK_OCCLUSION
      as isize,
  MAX = ll::FMOD_CHANNELCONTROL_CALLBACK_TYPE_FMOD_CHANNELCONTROL_CALLBACK_MAX
    as isize
}

/// Special types of nodes within a DSP chain.
///
/// By default there is one fader for a ChannelGroup or Channel and it is the
/// head.
pub enum DspIndex {
  /// -1: Head of the DSP chain (always index 0)
  Head  = ll::FMOD_CHANNELCONTROL_DSP_INDEX_FMOD_CHANNELCONTROL_DSP_HEAD
    as isize,
  /// -2: Index of the built-in fader DSP; initially 0
  Fader = ll::FMOD_CHANNELCONTROL_DSP_INDEX_FMOD_CHANNELCONTROL_DSP_FADER
    as isize,
  /// -3: Tail of the DSP chain (number of DSPs -1)
  Tail  = ll::FMOD_CHANNELCONTROL_DSP_INDEX_FMOD_CHANNELCONTROL_DSP_TAIL
    as isize
}

pub trait ChannelControl {
  fn add_dsp (&mut self, index : i32, dsp : &mut Dsp) -> Result <(), Error>;
  fn add_fade_point (&mut self, dspclock : u64, volume : f32)
    -> Result <(), Error>;
  fn get_3d_attributes (&self) -> Result <([f32; 3], [f32; 3]), Error>;
  fn get_3d_cone_orientation (&self) -> Result <[f32; 3], Error>;
  fn get_3d_cone_settings (&self) -> Result <(f32, f32, f32), Error>;
  fn get_3d_custom_rolloff (&self) -> Result <Vec <[f32; 3]>, Error>;
  fn get_3d_distance_filter (&self) -> Result <(bool, f32, f32), Error>;
  fn get_3d_doppler_level (&self) -> Result <f32, Error>;
  // TODO: fn get_3d_level
  fn get_3d_min_max_distance (&self) -> Result <(f32, f32), Error>;
  fn get_3d_occlusion (&self) -> Result <(f32, f32), Error>;
  fn get_3d_spread (&self) -> Result <f32, Error>;
  fn get_audibility (&self) -> Result <f32, Error>;
  fn get_delay (&self) -> Result <Delay, Error>;
  fn get_dsp (&self, index : i32) -> Result <DspRef, Error>;
  fn get_dsp_clock (&self) -> Result <u64, Error>;
  fn get_dsp_clock_parent (&self) -> Result <u64, Error>;
  fn get_dsp_index (&self, dsp : &Dsp) -> Result <i32, Error>;
  fn get_low_pass_gain (&self) -> Result <f32, Error>;
  //TODO: fn get_mix_matrix
  fn get_mode (&self) -> Result <Mode, Error>;
  fn get_mute (&self) -> Result <bool, Error>;
  fn get_num_dsps (&self) -> Result <u32, Error>;
  fn get_paused (&self) -> Result <bool, Error>;
  //TODO: fn get_pitch
  fn get_reverb_properties (&self, instance : i32) -> Result <f32, Error>;
  fn get_volume (&self) -> Result <f32, Error>;
  //TODO: fn get_volume_ramp
  fn is_playing (&self) -> Result <bool, Error>;
  fn remove_dsp (&mut self, dsp : &mut Dsp) -> Result <(), Error>;
  //TODO: fn remove_fade_points
  fn set_3d_attributes (&mut self, pos : [f32; 3], vel : [f32; 3])
    -> Result <(), Error>;
  //TODO: fn set_3d_cone_orientation
  //TODO: fn set_3d_cone_settings
  //TODO: fn set_3d_custom_rolloff
  //TODO: fn set_3d_distance_filter
  //TODO: fn set_3d_doppler_level
  //TODO: fn set_3d_level
  //TODO: fn set_3d_min_max_distance
  //TODO: fn set_3d_occlusion
  //TODO: fn set_3d_spread
  //TODO: fn set_callback
  fn set_delay (&mut self,
    dspclock_start : u64, dspclock_end : u64, stopchannels : bool
  ) -> Result <(), Error>;
  //TODO: fn set_dsp_index
  fn set_fade_point_ramp (&mut self, dspclock : u64, volume : f32)
    -> Result <(), Error>;
  //TODO: fn set_low_pass_gain
  //TODO: fn set_mix_levels_input
  //TODO: fn set_mix_levels_output
  //TODO: fn set_mix_matrix
  //TODO: fn set_mode
  fn set_mute (&mut self, mute : bool) -> Result <(), Error>;
  //TODO: fn set_pan
  fn set_paused (&mut self, paused : bool) -> Result <(), Error>;
  //TODO: fn set_pitch

  /// Set the wet level of a reverb instance.
  ///
  /// `instance` -- Index of the particular reverb instance to target, from 0 to
  /// `fmod::dsp::REVERB_MAXINSTANCES`.
  ///
  /// `wet` -- Send level for the signal to the reverb, from 0 (none) to 1.0
  /// (full), default = 1.0 for Channels, 0.0 for ChannelGroups.
  ///
  /// A Channel is automatically connected to all existing reverb instances due
  /// to the default wet level of 1.0. A ChannelGroup however will not send to
  /// any reverb by default requiring an explicit call to this function.
  ///
  /// A ChannelGroup reverb is optimal for the case where you want to send 1
  /// mixed signal to the reverb, rather than a lot of individual channel reverb
  /// sends. It is advisable to do this to reduce CPU if you have many Channels
  /// inside a ChannelGroup.
  ///
  /// Keep in mind when setting a wet level for a ChannelGroup, any Channels
  /// under that ChannelGroup will still have their existing sends to the
  /// reverb. To avoid this doubling up you should explicitly set the Channel
  /// wet levels to 0.0.
  fn set_reverb_properties (&mut self, instance : i32, wet : f32)
    -> Result <(), Error>;

  //TODO: fn set_user_data
  fn set_volume (&mut self, volume : f32) -> Result <(), Error>;
  //TODO: fn set_volume_ramp
  fn stop (&mut self) -> Result <(), Error>;

  // TODO: more methods
}
