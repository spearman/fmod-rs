use num_derive::FromPrimitive;
use crate::{ll};

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct Desc {
  pub type_       : Type,
  pub name        : String,
  pub label       : String,
  pub description : String
}

#[derive(Copy, Clone, Debug, Eq, PartialEq, FromPrimitive)]
pub enum Type {
  Bool  = ll::FMOD_DSP_PARAMETER_TYPE_FMOD_DSP_PARAMETER_TYPE_BOOL  as isize,
  Data  = ll::FMOD_DSP_PARAMETER_TYPE_FMOD_DSP_PARAMETER_TYPE_DATA  as isize,
  Float = ll::FMOD_DSP_PARAMETER_TYPE_FMOD_DSP_PARAMETER_TYPE_FLOAT as isize,
  Int   = ll::FMOD_DSP_PARAMETER_TYPE_FMOD_DSP_PARAMETER_TYPE_INT   as isize,
  MAX   = ll::FMOD_DSP_PARAMETER_TYPE_FMOD_DSP_PARAMETER_TYPE_MAX   as isize
}

// TODO: more parameters

#[derive(Copy, Clone, Debug, Eq, PartialEq, FromPrimitive)]
pub enum Echo {
  Delay    = ll::FMOD_DSP_ECHO_FMOD_DSP_ECHO_DELAY    as isize,
  Feedback = ll::FMOD_DSP_ECHO_FMOD_DSP_ECHO_FEEDBACK as isize,
  DryLevel = ll::FMOD_DSP_ECHO_FMOD_DSP_ECHO_DRYLEVEL as isize,
  WetLevel = ll::FMOD_DSP_ECHO_FMOD_DSP_ECHO_WETLEVEL as isize
}

#[derive(Copy, Clone, Debug, Eq, PartialEq, FromPrimitive)]
pub enum Lowpass {
  /// float 10.0-22000.0, default = 5000.0
  Cutoff    = ll::FMOD_DSP_LOWPASS_FMOD_DSP_LOWPASS_CUTOFF    as isize,
  /// float 1.0-10.0, default = 1.0
  Resonance = ll::FMOD_DSP_LOWPASS_FMOD_DSP_LOWPASS_RESONANCE as isize
}

#[derive(Copy, Clone, Debug, Eq, PartialEq, FromPrimitive)]
pub enum MultibandEq {
  /// int MultibandEqFilterType, default = Lowpass12db
  AFilter    = ll::FMOD_DSP_MULTIBAND_EQ_FMOD_DSP_MULTIBAND_EQ_A_FILTER as isize,
  /// float 20.0-22000.0, default = 8000.0
  AFrequency = ll::FMOD_DSP_MULTIBAND_EQ_FMOD_DSP_MULTIBAND_EQ_A_FREQUENCY
    as isize,
  /// float 0.1-10.0, default = 0.707
  AQ         = ll::FMOD_DSP_MULTIBAND_EQ_FMOD_DSP_MULTIBAND_EQ_A_Q as isize,
  /// float -30.0-30.0, default = 0.0
  AGain      = ll::FMOD_DSP_MULTIBAND_EQ_FMOD_DSP_MULTIBAND_EQ_A_GAIN as isize,
  /// int MultibandEqFilterType, default = Disabled
  BFilter    = ll::FMOD_DSP_MULTIBAND_EQ_FMOD_DSP_MULTIBAND_EQ_B_FILTER as isize,
  /// float 20.0-22000.0, default = 8000.0
  BFrequency = ll::FMOD_DSP_MULTIBAND_EQ_FMOD_DSP_MULTIBAND_EQ_B_FREQUENCY
    as isize,
  /// float 0.1-10.0, default = 0.707
  BQ         = ll::FMOD_DSP_MULTIBAND_EQ_FMOD_DSP_MULTIBAND_EQ_B_Q as isize,
  /// float -30.0-30.0, default = 0.0
  BGain      = ll::FMOD_DSP_MULTIBAND_EQ_FMOD_DSP_MULTIBAND_EQ_B_GAIN as isize,
  /// int MultibandEqFilterType, default = Disabled
  CFilter    = ll::FMOD_DSP_MULTIBAND_EQ_FMOD_DSP_MULTIBAND_EQ_C_FILTER as isize,
  /// float 20.0-22000.0, default = 8000.0
  CFrequency = ll::FMOD_DSP_MULTIBAND_EQ_FMOD_DSP_MULTIBAND_EQ_C_FREQUENCY
    as isize,
  /// float 0.1-10.0, default = 0.707
  CQ         = ll::FMOD_DSP_MULTIBAND_EQ_FMOD_DSP_MULTIBAND_EQ_C_Q as isize,
  /// float -30.0-30.0, default = 0.0
  CGain      = ll::FMOD_DSP_MULTIBAND_EQ_FMOD_DSP_MULTIBAND_EQ_C_GAIN as isize,
  /// int MultibandEqFilterType, default = Disabled
  DFilter    = ll::FMOD_DSP_MULTIBAND_EQ_FMOD_DSP_MULTIBAND_EQ_D_FILTER as isize,
  /// float 20.0-22000.0, default = 8000.0
  DFrequency = ll::FMOD_DSP_MULTIBAND_EQ_FMOD_DSP_MULTIBAND_EQ_D_FREQUENCY
    as isize,
  /// float 0.1-10.0, default = 0.707
  DQ         = ll::FMOD_DSP_MULTIBAND_EQ_FMOD_DSP_MULTIBAND_EQ_D_Q as isize,
  /// float -30.0-30.0, default = 0.0
  DGain      = ll::FMOD_DSP_MULTIBAND_EQ_FMOD_DSP_MULTIBAND_EQ_D_GAIN as isize,
  /// int MultibandEqFilterType, default = Disabled
  EFilter    = ll::FMOD_DSP_MULTIBAND_EQ_FMOD_DSP_MULTIBAND_EQ_E_FILTER as isize,
  /// float 20.0-22000.0, default = 8000.0
  EFrequency = ll::FMOD_DSP_MULTIBAND_EQ_FMOD_DSP_MULTIBAND_EQ_E_FREQUENCY
    as isize,
  /// float 0.1-10.0, default = 0.707
  EQ         = ll::FMOD_DSP_MULTIBAND_EQ_FMOD_DSP_MULTIBAND_EQ_E_Q as isize,
  /// float -30.0-30.0, default = 0.0
  EGain      = ll::FMOD_DSP_MULTIBAND_EQ_FMOD_DSP_MULTIBAND_EQ_E_GAIN as isize,
}

#[derive(Copy, Clone, Debug, Eq, PartialEq, FromPrimitive)]
pub enum MultibandEqFilterType {
  Disabled =
    ll::FMOD_DSP_MULTIBAND_EQ_FILTER_TYPE_FMOD_DSP_MULTIBAND_EQ_FILTER_DISABLED
    as isize,
  Lowpass12db =
    ll::FMOD_DSP_MULTIBAND_EQ_FILTER_TYPE_FMOD_DSP_MULTIBAND_EQ_FILTER_LOWPASS_12DB
    as isize,
  Lowpass24db =
    ll::FMOD_DSP_MULTIBAND_EQ_FILTER_TYPE_FMOD_DSP_MULTIBAND_EQ_FILTER_LOWPASS_24DB
    as isize,
  Lowpass48db =
    ll::FMOD_DSP_MULTIBAND_EQ_FILTER_TYPE_FMOD_DSP_MULTIBAND_EQ_FILTER_LOWPASS_48DB
    as isize,
  Highpass12db =
    ll::FMOD_DSP_MULTIBAND_EQ_FILTER_TYPE_FMOD_DSP_MULTIBAND_EQ_FILTER_HIGHPASS_12DB
    as isize,
  Highpass24db =
    ll::FMOD_DSP_MULTIBAND_EQ_FILTER_TYPE_FMOD_DSP_MULTIBAND_EQ_FILTER_HIGHPASS_24DB
    as isize,
  Highpass48db =
    ll::FMOD_DSP_MULTIBAND_EQ_FILTER_TYPE_FMOD_DSP_MULTIBAND_EQ_FILTER_HIGHPASS_48DB
    as isize,
  Lowshelf =
    ll::FMOD_DSP_MULTIBAND_EQ_FILTER_TYPE_FMOD_DSP_MULTIBAND_EQ_FILTER_LOWSHELF
    as isize,
  Highshelf =
    ll::FMOD_DSP_MULTIBAND_EQ_FILTER_TYPE_FMOD_DSP_MULTIBAND_EQ_FILTER_HIGHSHELF
    as isize,
  Peaking =
    ll::FMOD_DSP_MULTIBAND_EQ_FILTER_TYPE_FMOD_DSP_MULTIBAND_EQ_FILTER_PEAKING
    as isize,
  Bandpass =
    ll::FMOD_DSP_MULTIBAND_EQ_FILTER_TYPE_FMOD_DSP_MULTIBAND_EQ_FILTER_BANDPASS
    as isize,
  Notch =
    ll::FMOD_DSP_MULTIBAND_EQ_FILTER_TYPE_FMOD_DSP_MULTIBAND_EQ_FILTER_NOTCH
    as isize,
  Allpass =
    ll::FMOD_DSP_MULTIBAND_EQ_FILTER_TYPE_FMOD_DSP_MULTIBAND_EQ_FILTER_ALLPASS
    as isize
}

/// See also `reverb3d::Properties`
#[derive(Copy, Clone, Debug, Eq, PartialEq, FromPrimitive)]
pub enum Sfxreverb {
  DecayTime    = ll::FMOD_DSP_SFXREVERB_FMOD_DSP_SFXREVERB_DECAYTIME   as isize,
  Density      = ll::FMOD_DSP_SFXREVERB_FMOD_DSP_SFXREVERB_DENSITY     as isize,
  Diffusion    = ll::FMOD_DSP_SFXREVERB_FMOD_DSP_SFXREVERB_DIFFUSION   as isize,
  DryLevel     = ll::FMOD_DSP_SFXREVERB_FMOD_DSP_SFXREVERB_DRYLEVEL    as isize,
  EarlyDelay   = ll::FMOD_DSP_SFXREVERB_FMOD_DSP_SFXREVERB_EARLYDELAY  as isize,
  EarlyLateMix = ll::FMOD_DSP_SFXREVERB_FMOD_DSP_SFXREVERB_EARLYLATEMIX as isize,
  HfDecayRatio = ll::FMOD_DSP_SFXREVERB_FMOD_DSP_SFXREVERB_HFDECAYRATIO as isize,
  HfReference  = ll::FMOD_DSP_SFXREVERB_FMOD_DSP_SFXREVERB_HFREFERENCE as isize,
  HighCut      = ll::FMOD_DSP_SFXREVERB_FMOD_DSP_SFXREVERB_HIGHCUT     as isize,
  LateDelay    = ll::FMOD_DSP_SFXREVERB_FMOD_DSP_SFXREVERB_LATEDELAY   as isize,
  LowShelfFrequency = ll::FMOD_DSP_SFXREVERB_FMOD_DSP_SFXREVERB_LOWSHELFFREQUENCY
    as isize,
  LowShelfGain = ll::FMOD_DSP_SFXREVERB_FMOD_DSP_SFXREVERB_LOWSHELFGAIN as isize,
  WetLevel     = ll::FMOD_DSP_SFXREVERB_FMOD_DSP_SFXREVERB_WETLEVEL    as isize
}

