extern crate bindgen;

fn main() {
  let bindings = bindgen::Builder::default()
    .header ("wrapper.h")
    .clang_arg ("-I./include")
    .derive_debug (false)
    .generate()
    .unwrap();
  let out_path = std::path::PathBuf::from (std::env::var ("OUT_DIR").unwrap());
  bindings.write_to_file (out_path.join ("bindings.rs")).unwrap();
  let libdir = format!("{}/lib/x86_64",
    std::env::var ("CARGO_MANIFEST_DIR").unwrap());
  println!("cargo:rustc-link-search=native={}", libdir);
}
