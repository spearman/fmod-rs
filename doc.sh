#!/bin/sh
set -x

cargo doc --all --no-deps --open

exit 0
