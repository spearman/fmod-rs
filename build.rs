fn main() {
  // link -lfmod on macos; not required for linux, and will fail on windows
  #[cfg(target_os = "macos")]
  println!("cargo:rustc-link-lib=dylib=fmod");
  // where to find libfmod.so or libfmod.dylib on linux or macos; on windows,
  // fmod64.dll should be placed in the local directory
  if let Some (library_path_var) = if cfg!(target_os = "linux") {
    Some ("LD_LIBRARY_PATH")
  } else if cfg!(target_os = "macos") {
    Some ("DYLD_LIBRARY_PATH")
  } else {
    None
  } {
    let library_path = std::env::var (library_path_var)
      .map (|p| format!("{}:", p)).unwrap_or ("".to_string());
    println!("cargo:rustc-env={}={}./fmod-sys/lib/x86_64",
      library_path_var, library_path);
  }
}
