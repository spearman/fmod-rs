#!/usr/bin/env bash

set -x

cargo run --example example -- $@

exit
